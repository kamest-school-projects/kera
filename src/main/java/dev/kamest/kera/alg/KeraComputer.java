package dev.kamest.kera.alg;

import Jama.Matrix;
import dev.kamest.kera.model.IdProvider;
import dev.kamest.kera.model.KeraItem;
import dev.kamest.kera.model.KeraOpinion;
import dev.kamest.kera.model.KeraResultItem;
import dev.kamest.kera.model.internal.KeraMatrixModel;
import dev.kamest.kera.prov.ext.ItemsProvider;
import dev.kamest.kera.prov.ext.OpinionsProvider;
import dev.kamest.kera.prov.ext.UsersProvider;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import javax.annotation.Nullable;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Base class that holds all information about model and current evalution of computer.
 * <p>
 * All logic should be called from this abstract class to children,
 * but in case of overriding or special functionality of external recommender,
 * feel free to use this class only as handler of data
 * <p>
 * This code is part of bachelor thesis:
 * Application of machine learning algorithms on purchase recommendation
 * UHK FIM KIT
 *
 * @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 **/
@RequiredArgsConstructor
@Data
public abstract class KeraComputer {

    /**
     * Indicates whether data are ranking (0/1,bought/unknown,visited/unknown) or rating (0-5,0-10,0-100).
     */
    @Getter
    private final Boolean isRanking;
    /**
     * Controls how many similar users or items should have affect on result.
     */
    @Setter
    protected int neighborhoodSize = 100;
    /**
     * Provides items from external system
     */
    protected ItemsProvider itemsProvider;

    /**
     * Provides users from external system
     */
    protected UsersProvider usersProvider;

    /**
     * Provides opinions from external system
     */
    protected OpinionsProvider opinionsProvider;


    /**
     * Holds base trained model.
     */
    private KeraMatrixModel model;

    /**
     * User index holds external ids looks like:
     * [21548,"Stepan Kamenik"],[245874,"Jan Novak"]
     * For performance exists second index
     */
    private Map<Integer, String> userIndex = new HashMap<>();
    private Map<String,Integer> userExtIndex = new HashMap<>();

    /**
     * Item index holds external ids looks like:
     * [147,"White jacket"],[6574,"Black shirt"]
     * For performance exists second index
     */
    private Map<Integer, String> itemIndex = new HashMap<>();
    private Map<String, Integer> itemExtIndex = new HashMap<>();

    /**
     * Tag index holds external ids looks like:
     * [8745,"red"],[689,"size XS"]
     * For performance exists second index
     */
    private Map<Integer, String> tagIndex = new HashMap<>();
    private Map<String,Integer> tagExtIndex = new HashMap<>();

    /**
     * User matrix produced by matrix decomposition.
     */
    private Matrix userMatrix;
    /**
     * Item matrix produced by matrix decomposition.
     */
    private Matrix itemMatrix;

    /**
     * Weights produced by matrix decomposition - latent factors.
     */
    private RealVector weight;

    /**
     * User bias map. (more info in computers)
     */
    private Map<Integer, Double> userBias = new HashMap<>();

    /**
     * Item bias map. (more info in computers)
     */
    private Map<Integer, Double> itemBias = new HashMap<>();

    /**
     * Global bias. (more info in computers)
     */
    private double globalBias = 0.0;

    /**
     * Computes / trains model.
     */
    public void computeModel() {
        clearCache();
        setModel(null);
        this.model = computeModel(isRanking);
        computeExtModel(isRanking);
    }

    /**
     * Set providers.
     */
    public void initVars(ItemsProvider itemsProvider, UsersProvider usersProvider, OpinionsProvider opinionsProvider) {
        this.itemsProvider = itemsProvider;
        this.usersProvider = usersProvider;
        this.opinionsProvider = opinionsProvider;
    }

    public abstract KeraMatrixModel computeModel(Boolean isRanking);

    /**
     * In case of MF , computation results {@link RealMatrix}ces,
     * so standard method {@link KeraComputer#computeModel()} is useless.
     * In that case use this method that is called in the same time and leave previous method to null.
     */
    public void computeExtModel(Boolean isRanking) {
        // only for external algs
    }

    /**
     * Basic method for recommendation,
     * recommends best n items for user userId
     *
     * @param userId user for whom we create recommendations
     * @param n number of recommended items
     * @param forbidKnownItems indicates whether known (referenced and history items) could be recommended.
     * @return list of recommended items
     */
    public abstract List<KeraResultItem> recommend(String userId, int n, boolean forbidKnownItems);

    /**
     * Recommends with referenced items, mostly fins similar to referenced items, not only for history opinions.
     *
     * @param userId user for whom we create recommendations
     * @param n number of recommended items
     * @param referenceItems items that should recommends be similar to.
     * @param forbidKnownItems indicates whether known (referenced and history items) could be recommended.
     * @return list of recommended items
     */
    public abstract List<KeraResultItem> recommend(String userId, int n, List<KeraItem> referenceItems, boolean forbidKnownItems);

    /**
     * Use this method in case that computer generates any aditional data in training and needs them in recommendation.
     */
    public Map<Integer, Double> getModelAdditionalData() {
        return null;
    }

    /**
     * Retrieves back additional data from stored version.
     */
    public void setModelAdditionalData(Map<Integer, Double> additionalData) {
        // ignored
    }

    /**
     * @return items that should not occur in recommentation list,
     * items that was reated / bought before and referenced items.
     * @param userId userId for whom recommendation is
     * @param forbidKnownItems indicates if items should be permited or not.
     */
    protected List<String> getForbiddenItems(String userId, List<KeraItem> referenceItems, boolean forbidKnownItems) {
        return forbidKnownItems
                ? Stream.concat(
                opinionsProvider.getOpinionsFromUser(userId).stream().map(KeraOpinion::getItemId),
                referenceItems.stream().map(IdProvider::getExternalId))
                .collect(Collectors.toList())
                : Collections.emptyList();
    }

    /**
     * Cleares all stored data.
     */
    protected void clearCache() {
        itemIndex.clear();
        userIndex.clear();
        tagIndex.clear();
        itemExtIndex.clear();
        userExtIndex.clear();
        tagExtIndex.clear();

        userBias.clear();
        itemBias.clear();
        globalBias = 0.0;
    }
    // Util methods

    public Integer addUserToIndex(String externalId) {
        Integer integer = userExtIndex.get(externalId);
        if(integer == null){
            integer = userExtIndex.size();
            userExtIndex.put(externalId, integer);
            userIndex.put(integer,externalId);
        }
        return integer;
    }

    public Integer addItemToIndex(String externalId) {
        Integer integer = itemExtIndex.get(externalId);
        if(integer == null){
            integer = itemExtIndex.size();
            itemExtIndex.put(externalId, integer);
            itemIndex.put(integer,externalId);
        }
        return integer;
    }
    public Integer addTagToIndex(String externalId) {
        Integer integer = tagExtIndex.get(externalId);
        if(integer == null){
            integer = tagExtIndex.size();
            tagExtIndex.put(externalId, integer);
            tagIndex.put(integer,externalId);
        }
        return integer;
    }

    public String getUserId(Integer user) {
        return userIndex.get(user);
    }

    @Nullable
    public Integer getUserId(String user) {
        return userExtIndex.get(user);
    }

    public String getItemId(Integer item) {
        return itemIndex.get(item);
    }

    @Nullable
    public Integer getItemId(String item) {
        return itemExtIndex.get(item);
    }

    public String getTagId(Integer tag) {
        return tagIndex.get(tag);
    }

    @Nullable
    public Integer getTagId(String tag) {
        return tagExtIndex.get(tag);
    }

    public Boolean isRanking() {
        return isRanking;
    }


}
