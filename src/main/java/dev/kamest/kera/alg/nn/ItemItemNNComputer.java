package dev.kamest.kera.alg.nn;

import com.google.common.util.concurrent.AtomicDouble;
import dev.kamest.kera.alg.KeraComputer;
import dev.kamest.kera.alg.VectorUtils;
import dev.kamest.kera.model.KeraItem;
import dev.kamest.kera.model.KeraOpinion;
import dev.kamest.kera.model.KeraResultItem;
import dev.kamest.kera.model.internal.KeraMatrixModel;
import lombok.Getter;
import lombok.extern.apachecommons.CommonsLog;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Basic Item-Item based recommendation computer,
 * uses cosine similarity, but that could be easily replaced with other method (Pearson,etc..)
 * <p>
 * Look at the methods doc.
 * <p>
 * In ranking case, see also {@link dev.kamest.kera.alg.stat.ItemAssociationComputer},
 * matrices are pretty similar, but recommendation methods are really different.
 * <p>
 * Refs:
 * https://md.ekstrandom.net/blog/2015/06/item-similarity
 * https://www.researchgate.net/publication/200121014_Item-based_collaborative_filtering_recommendation_algorithmus
 * <p>
 * This code is part of bachelor thesis:
 * Application of machine learning algorithms on purchase recommendation
 * UHK FIM KIT
 *
 * @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 **/
@CommonsLog
public class ItemItemNNComputer extends KeraComputer {

    /**
     * Contain mean opinion scores per item (0-5 in case of 5 rate)
     */
    @Getter
    private Map<Integer, Double> itemMeans = new HashMap<>();

    public ItemItemNNComputer(boolean isRanking) {
        super(isRanking);
    }

    public static boolean needsRegisteredUsers() {
        return false;
    }

    /**
     * Recommendation is computed as highest sum of neighbors,
     * in other words,
     * <p>
     * In matrix we have items and similarity index with other items,
     * so in this method we retrieve all items on eshop and for each item look at the model matrix,
     * If item has neighbors (similar items from model) in referenceItems we compute sum of their values (value of cosine similarity),
     * then we sort list of computed sums and pick top n with the largest score of similarity with referenceItems.
     *
     * @param userId           user for whom we create recommendations
     * @param n                number of recommended items
     * @param referenceItems   items that should recommends be similar to.
     * @param forbidKnownItems indicates whether known (referenced and history items) could be recommended.
     * @return list of recommended items
     */
    @Override
    public List<KeraResultItem> recommend(String userId, int n, List<KeraItem> referenceItems, boolean forbidKnownItems) {
        List<KeraResultItem> results = new ArrayList<>(n);

        List<String> forbiddenItems = getForbiddenItems(userId, referenceItems, forbidKnownItems);
        itemsProvider.getObjects().forEach(item -> {
            // forces known items out
            if (forbiddenItems.contains(item.getExternalId())) return;

            // Retrieves similar items with one in local loop - item
            Map<Integer, Double> neighbors = getModel()
                    .get(getItemId(item.getExternalId()));

            results.add(
                    new KeraResultItem(
                            item,
                            referenceItems
                                    .stream()
                                    .map(itemB -> getItemId(itemB.getExternalId()))
                                    .filter(key -> key != null && neighbors.containsKey(key))
                                    .mapToDouble(neighbors::get)
                                    .sum()
                    )
            );
        });

        List<String> usedMasters = new LinkedList<>();
        return results
                .stream()
                .sorted(Collections.reverseOrder(Comparator.comparing(KeraResultItem::getScore)))
                .filter(i->{
                    String masterId = i.getMasterId();
                    if (masterId == null) return true;
                    if (usedMasters.contains(masterId)) return false;
                    usedMasters.add(masterId);
                    return true;
                })
                .limit(n)
                .collect(Collectors.toList());
    }

    /**
     * The same as dev.kamest.kera.alg.nn.ItemItemNNComputer#recommend(java.lang.String, int, java.util.List, boolean),
     * but in this case weights similar items score with possible opinion from current user.
     *
     * @param userId           user for whom we create recommendations
     * @param n                number of recommended items
     * @param forbidKnownItems indicates whether known (referenced and history items) could be recommended.
     * @return list of recommended items
     */
    @Override
    public List<KeraResultItem> recommend(String userId, int n, boolean forbidKnownItems) {

        Map<Integer, Double> itemMeans = getItemMeans();
        Map<Integer, Double> opinions = Optional.ofNullable(opinionsProvider
                .getOpinionsFromUser(userId))
                .orElse(Collections.emptyList())
                .stream()
                .collect(Collectors.toMap(i -> getItemId(i.getItemId()), KeraOpinion::getOpinion,(i, j)->i.compareTo(j) >= 0 ? i : j));


        // If user is not known and we don't have any referenced items, we cannot recommend
        if (opinions.isEmpty()) return Collections.emptyList();

        if (!isRanking()) {
            // Normalize opinions with each item mean
            opinions.entrySet()
                    .stream()
                    .filter(obj -> Objects.nonNull(obj.getKey()))
                    .forEach(entry -> entry.setValue(entry.getValue() - Optional.ofNullable(itemMeans.get(entry.getKey())).orElse(0.0)));
        }

        List<KeraResultItem> results = new ArrayList<>();
        itemsProvider.getObjects().forEach(i -> {
            Integer itemExtId = getItemId(i.getExternalId());
            // Cannot recommend unknown item
            if (opinions.containsKey(itemExtId)) return;
            // Get similar to this item
            Map<Integer, Double> itemNeighbors = getModel().get(itemExtId);

            if (itemNeighbors == null) return;
            Set<Map.Entry<Integer, Double>> entrySet = itemNeighbors.entrySet();
            AtomicDouble weightSum = new AtomicDouble(0);
            AtomicDouble ratingSum = new AtomicDouble(0);
            entrySet
                    .stream()
                    .filter(j -> opinions.containsKey(j.getKey()))
                    .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                    .limit(neighborhoodSize)
                    .forEach(j -> {
                        Double weight = j.getValue();
                        // Weight neighbors with known opinion from this user
                        ratingSum.addAndGet(isRanking() ? weight : weight * opinions.get(j.getKey()));
                        weightSum.addAndGet(weight);
                    });
            if (ratingSum.get() == 0) return;
            // Normalize score in case of ratings , in case of ranking only push ratingSum
            double score =
                    isRanking() ? ratingSum.get()
                            : Optional.ofNullable(itemMeans.get(itemExtId)).orElse(0.0) + ratingSum.get()
                                / weightSum.get();

            results.add(new KeraResultItem(i, score));
        });

        List<String> usedMasters = new LinkedList<>();
        return results
                .stream()
                .sorted(Collections.reverseOrder(Comparator.comparingDouble(KeraResultItem::getScore)))
                .filter(i->{
                    String masterId = i.getMasterId();
                    if (masterId == null) return true;
                    if (usedMasters.contains(masterId)) return false;
                    usedMasters.add(masterId);
                    return true;
                })
                .limit(n)
                .collect(Collectors.toList());
    }

    /**
     * Computes model that contains items x items matrix,
     * for each item computes similarity score with each other item.
     *
     * @param isRanking indicates whether normalization should be applied
     * @return trained matrix model
     */
    @Override
    public KeraMatrixModel computeModel(Boolean isRanking) {

        // clear
        itemMeans.clear();

        Map<Integer, Map<Integer, Double>> itemVectors = new HashMap<>();
        Map<Integer, Map<Integer, Double>> itemsOpinions = new HashMap<>();
        List<KeraOpinion> opinions = opinionsProvider.getObjects();

        // Creates map of users that rated (opinionated) item Map<ItemId,Map<UserId, OpinionValue>>
        opinions.forEach(i -> {
            Integer index = addItemToIndex(i.getItemId());
            Map<Integer, Double> itemOpinions = itemsOpinions.getOrDefault(index, new HashMap<>());
            itemOpinions.put(addUserToIndex(i.getUserId()), i.getOpinion());
            itemsOpinions.put(index, itemOpinions);
        });

        if (!isRanking) {
            // Only if opinions are ratings , in case of ranking there's no need to normalize (only values are 0s and 1s)
            itemsOpinions.forEach((itemKey, itemOpinion) -> {

                // Compute mean of item opinions - item vector
                double mean = VectorUtils.mean(itemOpinion);
                itemMeans.put(itemKey, mean);

                // Normalize item vector (opinion - mean)
                itemOpinion.entrySet().forEach(i -> i.setValue(i.getValue() - mean));
                itemVectors.put(itemKey, itemOpinion);
            });
        }else {
            // Only if opinions are ratings , in case of ranking there's no need to normalize (only values are 0s and 1s)
            itemsOpinions.forEach(itemVectors::put);
        }

        // Create final model
        KeraMatrixModel model = new KeraMatrixModel();
        itemVectors.forEach((key1, value1) -> {
            Map<Integer, Double> similarities = new HashMap<>();
            itemVectors.forEach((key, value) -> {
                if (key.equals(key1)) return;

                // Computes cosine similarity
                double sumUV = VectorUtils.dotProduct(value1, value);
                double similarity = sumUV / (VectorUtils.euclideanNorm(value1) * VectorUtils.euclideanNorm(value));
                // Only if items are at least a bit similar, we don't want to recommend upon negativity (
                if (similarity > 0) {
                    similarities.put(key, similarity);
                }
            });


            if (!similarities.isEmpty()) {
                model.put(
                        key1,
                        similarities
                                .entrySet()
                                .stream()
                                // We could limit neighborhood size but it decreases metrics a lot!
                                // .sorted(Collections.reverseOrder(Comparator.comparingDouble(Map.Entry::getValue)))
                                // .limit(n)
                                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue))
                );
            }

        });

        return model;
    }

    /**
     * @see KeraComputer#getModelAdditionalData()
     */
    @Override
    public Map<Integer, Double> getModelAdditionalData() {
        return itemMeans;
    }

    /**
     * @see KeraComputer#setModelAdditionalData(java.util.Map)
     */
    public void setModelAdditionalData(Map<Integer, Double> additionalData) {
        itemMeans = additionalData;
    }


}
