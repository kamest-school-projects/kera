package dev.kamest.kera.alg.stat;

import com.google.common.util.concurrent.AtomicDouble;
import dev.kamest.kera.alg.nn.ItemItemNNComputer;
import dev.kamest.kera.model.KeraItem;
import dev.kamest.kera.model.KeraOpinion;
import dev.kamest.kera.model.KeraResultItem;
import dev.kamest.kera.model.internal.KeraMatrixModel;
import lombok.extern.apachecommons.CommonsLog;
import org.checkerframework.checker.units.qual.min;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

/**
 * Computes Association rules for products.
 * This algorithm is pretty simple and is called confidential association rule.
 * <p>
 * Rule is based on creation matrix where for each item
 * is set list of other items that are similar for users
 * that means users who bought/rated itemx also bought/rated item y.
 * <p>
 * In other words model computes probability that when user x bought some item x,
 * what probability has to buy item y.
 * <p>
 * Refs:
 * https://en.wikipedia.org/wiki/Association_rule_learning#Confidence
 * https://www.researchgate.net/publication/220788037_Association_Rules_Induced_by_Item_and_Quantity_Purchased
 * https://www.researchgate.net/publication/334329824_Summary_of_Association_Rules
 *
 * <p>
 * This code is part of bachelor thesis:
 * Application of machine learning algorithms on purchase recommendation
 * UHK FIM KIT
 *
 * @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 **/
@CommonsLog
public class ItemAssociationComputer extends ItemItemNNComputer {

    public ItemAssociationComputer(boolean isRanking) {
        super(isRanking);
    }


    public static boolean needsRegisteredUsers() {
        return false;
    }

    /**
     * Retrieve score of item {@link ItemAssociationComputer#getScore(KeraItem, Map)}.
     * Transform scale and limit to only top n.
     *
     * @param userId           user for whom we create recommendations
     * @param n                number of recommended items
     * @param forbidKnownItems indicates whether known (referenced and history items) could be recommended.
     * @return recommended items
     */
    @Override
    public List<KeraResultItem> recommend(String userId, int n, boolean forbidKnownItems) {
        // init indexes
        Map<Integer, Double> opinions = Optional.ofNullable(opinionsProvider
                .getOpinionsFromUser(userId))
                .orElse(Collections.emptyList())
                .parallelStream()
                .collect(Collectors.toMap(i -> getItemId(i.getItemId()), KeraOpinion::getOpinion));
        if (opinions.isEmpty()) return Collections.emptyList();
        List<KeraResultItem> results = new ArrayList<>();

        List<String> forbiddenItems = getForbiddenItems(userId, Collections.emptyList(), forbidKnownItems);
        AtomicDouble max = new AtomicDouble();
        AtomicDouble min = new AtomicDouble();
        itemsProvider
                .getObjects()
                .stream()
                .filter(i -> !forbiddenItems.contains(i.getExternalId()))
                .forEach(i -> {
                    double itemScore = getScore(i, opinions);
                    max.set(Math.max(itemScore, max.get()));
                    min.set(Math.min(itemScore, min.get()));
                    results.add(
                            new KeraResultItem(
                                    i,
                                    itemScore
                            )
                    );
                });
        // Normalize on range for evaluations
        final double finalMax = max.get();
        final double finalMin = min.get();
        List<String> usedMasters = new LinkedList<>();
        return results
                .stream()
                .sorted(Collections.reverseOrder(Comparator.comparingDouble(KeraResultItem::getScore)))
                .filter(i->{
                    String masterId = i.getMasterId();
                    if (masterId == null) return true;
                    if (usedMasters.contains(masterId)) return false;
                    usedMasters.add(masterId);
                    return true;
                })
                .peek(i -> i.setScore((5 / (finalMax - finalMin)) * (i.getScore() - finalMin)))
                .limit(n)
                .collect(Collectors.toList());
    }

    @Override
    public KeraMatrixModel computeModel(Boolean isRanking) {

        //Create work indexes
        Map<Integer, List<Integer>> itemUsers = new HashMap<>();
        List<KeraOpinion> rawOpinions = opinionsProvider.getObjects();
        rawOpinions.forEach(i -> {
            Integer itemIndex = addItemToIndex(i.getItemId());

            // Create item - users index
            Integer userIndex = addUserToIndex(i.getUserId());
            List<Integer> iu = itemUsers.getOrDefault(itemIndex, new LinkedList<>());
            if (!iu.contains(userIndex)) iu.add(userIndex);
            itemUsers.put(itemIndex, iu);
        });

        // Measurement utils
        int size = itemUsers.size();
        long init = System.currentTimeMillis();
        AtomicLong avgTime = new AtomicLong();

        KeraMatrixModel keraMatrixModel = new KeraMatrixModel();
        AtomicInteger itemUsersCount = new AtomicInteger(0);

        // For each item computes similar items
        itemUsers.forEach((key, value) -> {
            int itemUsersIndex = itemUsersCount.incrementAndGet();
            if (itemUsersIndex % 100 == 0) log.info("items " + itemUsersIndex + " from " + size);

            Map<Integer, Double> itemScores = new HashMap<>();
            itemUsers.forEach((key1, value1) -> {
                AtomicInteger countOfCommonUsers = new AtomicInteger(0);
                // Compare iu matrix and get count of users that rated / bought both items
                countOfCommonUsers.addAndGet((int) value.stream().filter(value1::contains).count());
                // "Normalize" for avoiding situation that one item is highly bought and one almost never but they has same users
                itemScores.put(key1, countOfCommonUsers.get() / (double) value.size());
            });

            // Put only high valued tom n items
            keraMatrixModel.put(
                    key,
                    itemScores
                            .entrySet()
                            .stream()
                            .sorted(Collections.reverseOrder(Comparator.comparingDouble(Map.Entry::getValue)))
                            .limit(neighborhoodSize)
                            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));

        });
        log.info("Computed in : " + TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis() - init) + "m with average in one item turn: " + TimeUnit.MILLISECONDS.toSeconds(avgTime.get() / size));

        return keraMatrixModel;
    }

    /**
     * Computes final score for potential item on base on its itemNeighbors from model matrix
     *
     * @param item     potential item
     * @param opinions known opinions from user to compute weight
     * @return score of item
     */
    private double getScore(KeraItem item, Map<Integer, Double> opinions) {
        Integer itemExtId = getItemId(item.getExternalId());
        Map<Integer, Double> itemNeighbors = getModel().get(itemExtId);
        // Item is not known by model
        if (itemNeighbors == null) return 0;

        AtomicInteger count = new AtomicInteger(1);
        return itemNeighbors
                .entrySet()
                .parallelStream()
                .filter(i -> opinions.containsKey(i.getKey()))
                .distinct()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .limit(neighborhoodSize)
                .mapToDouble(j -> {
                    count.incrementAndGet();
                    return isRanking() ? j.getValue() : j.getValue() * opinions.get(j.getKey());
                }).sum();
    }

}
