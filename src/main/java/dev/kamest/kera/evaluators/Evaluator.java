package dev.kamest.kera.evaluators;

import lombok.Data;

import java.util.Map;

/**
 *  Interface for all evaluators on that computer could be test.
 *
 *  This code is part of bachelor thesis:
 *  Application of machine learning algorithms on purchase recommendation
 *  UHK FIM KIT
 *
 *  @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 *
 **/
@Data
public abstract class Evaluator {

    private int topN;

    /**
     * Evaluate computers predicted (recommended) set on real test data.
     * @param testSet set that was not present in training set and could be tested with predictions
     * @param recommendations that could be served to user and are from {@link dev.kamest.kera.alg.KeraComputer#recommend(String, int, boolean)} or alternative methods.
     * @return measurement unit
     */
    public abstract double eval(Map<String, Map<String, Double>> testSet, Map<String, Map<String, Double>> recommendations);
}
