package dev.kamest.kera.evaluators.precision;

import com.google.common.util.concurrent.AtomicDouble;
import dev.kamest.kera.evaluators.Evaluator;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Reciprocal rank
 *
 * Refs:
 * https://en.wikipedia.org/wiki/Mean_reciprocal_rank
 * https://medium.com/swlh/rank-aware-recsys-evaluation-metrics-5191bba16832
 * <p>
 * This code is part of bachelor thesis:
 * Application of machine learning algorithms on purchase recommendation
 * UHK FIM KIT
 *
 * @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 **/
public class RR extends Evaluator {

    @Override
    public double eval(Map<String, Map<String, Double>> testSet, Map<String, Map<String, Double>> recommendations) {
        AtomicDouble rr = new AtomicDouble(0.0);
        AtomicInteger evals = new AtomicInteger();

        testSet.forEach((key1, value) -> {
            if (value.keySet().size() > 0) {

                // sort as for user
                List<String> sortedRecommendations = recommendations
                        .get(key1)
                        .entrySet()
                        .stream()
                        .sorted(Collections.reverseOrder(Comparator.comparingDouble(Map.Entry::getValue)))
                        .map(Map.Entry::getKey)
                        .collect(Collectors.toList());

                int top = Math.min(getTopN(), sortedRecommendations.size());
                for (double i = 1; i < top+1; ++i) {
                    if (value.containsKey(sortedRecommendations.get(((int)i-1)))) {
                        rr.addAndGet(1.0d / i);
                        break;
                    }
                }
                evals.getAndIncrement();
            }
        });
        return rr.get() / evals.get();
    }
}
