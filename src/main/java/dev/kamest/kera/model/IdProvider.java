package dev.kamest.kera.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * High ancestor of external objects that often has external id.
 *
 * <p>
 * This code is part of bachelor thesis:
 * Application of machine learning algorithms on purchase recommendation
 * UHK FIM KIT
 *
 * @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 **/
@Data
@RequiredArgsConstructor
public abstract class IdProvider {

    private final String externalId;

    public String getId() {
        return externalId;
    }

}
