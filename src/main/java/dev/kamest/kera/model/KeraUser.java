package dev.kamest.kera.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Stands as user in external service,
 * user could be registered or anonymous, but has to have some id.
 *
 * <p>
 * This code is part of bachelor thesis:
 * Application of machine learning algorithms on purchase recommendation
 * UHK FIM KIT
 *
 * @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 **/
@EqualsAndHashCode(callSuper = true)
public class KeraUser extends IdProvider implements KeraExtObject {

    public KeraUser(String externalId) {
        super(externalId);
        this.registered = true;
    }

    public KeraUser(String externalId, boolean registered) {
        super(externalId);
        this.registered = registered;
    }

    @Getter
    @Setter
    private boolean registered;
}
