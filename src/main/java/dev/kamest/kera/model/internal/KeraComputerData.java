package dev.kamest.kera.model.internal;

import Jama.Matrix;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import dev.kamest.kera.alg.KeraComputer;
import dev.kamest.kera.model.internal.util.LocalDateTimeDeserializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Simple dto for storing computer data to json
 * and store on file system for later loading.
 * (in case of restart, crash,etc.)
 *
 * <p>
 * This code is part of bachelor thesis:
 * Application of machine learning algorithms on purchase recommendation
 * UHK FIM KIT
 *
 * @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 **/
@AllArgsConstructor
@NoArgsConstructor
@Data
public class KeraComputerData {
    @JsonSerialize(using = ToStringSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime created = LocalDateTime.now() ;
    private KeraMatrixModel model;
    private Map<Integer,String> itemIndex;
    private Map<Integer,String> userIndex;
    private Map<Integer, String> tagIndex;
    private Map<Integer, Double> modelAdditionalData;

    private double[][] userMatrix;
    private double[][] itemMatrix;
    private double[] weight;

    private Map<Integer,Double> userBias = new HashMap<>();
    private Map<Integer,Double> itemBias = new HashMap<>();
    private double globalBias = 0.0;

    public KeraComputerData(KeraComputer computer) {
        this.model = computer.getModel();
        this.userMatrix = Optional.ofNullable(computer.getUserMatrix()).map(Matrix::getArray).orElse(null);
        this.itemMatrix = Optional.ofNullable(computer.getItemMatrix()).map(Matrix::getArray).orElse(null);
        this.weight = Optional.ofNullable(computer.getWeight()).map(i->((ArrayRealVector)i).getDataRef()).orElse(null);
        this.itemIndex = computer.getItemIndex();
        this.userIndex = computer.getUserIndex();
        this.tagIndex = computer.getTagIndex();
        this.modelAdditionalData = computer.getModelAdditionalData();
        this.userBias = computer.getUserBias();
        this.itemBias = computer.getItemBias();
        this.globalBias = computer.getGlobalBias();
    }

}
