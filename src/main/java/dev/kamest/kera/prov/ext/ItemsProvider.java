package dev.kamest.kera.prov.ext;

import dev.kamest.kera.model.KeraItem;

/**
 * Interface that provides external entity of type {@link KeraItem} to Kera library.
 * This interface has to be implemented and set to {@link dev.kamest.kera.Kera} service in creation.
 *
 * <p>
 * This code is part of bachelor thesis:
 * Application of machine learning algorithms on purchase recommendation
 * UHK FIM KIT
 *
 * @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 **/
public interface ItemsProvider extends KeraExtProvider<KeraItem> {

}
