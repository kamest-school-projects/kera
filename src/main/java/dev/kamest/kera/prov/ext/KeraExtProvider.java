package dev.kamest.kera.prov.ext;

import dev.kamest.kera.model.KeraExtObject;
import dev.kamest.kera.model.KeraItem;

import java.util.List;

/**
 * Interface that is accessor for external entity provider.
 * See specific providers itself.
 *
 * <p>
 * This code is part of bachelor thesis:
 * Application of machine learning algorithms on purchase recommendation
 * UHK FIM KIT
 *
 * @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 **/
public interface KeraExtProvider<T extends KeraExtObject> {

    List<T> getObjects();
}
