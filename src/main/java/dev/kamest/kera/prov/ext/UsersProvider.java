package dev.kamest.kera.prov.ext;

import dev.kamest.kera.model.KeraUser;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Abstract that provides external entity of type {@link KeraUser} to Kera library.
 * This interface has to be implemented and set to {@link dev.kamest.kera.Kera} service in creation.
 *
 * <p>
 * This code is part of bachelor thesis:
 * Application of machine learning algorithms on purchase recommendation
 * UHK FIM KIT
 *
 * @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 **/
@AllArgsConstructor
public abstract class UsersProvider implements KeraExtProvider<KeraUser> {

    @Getter
    @Setter
    private boolean onlyRegistered;

}
