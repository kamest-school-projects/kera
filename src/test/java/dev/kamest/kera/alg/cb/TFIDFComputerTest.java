package dev.kamest.kera.alg.cb;

import dev.kamest.kera.Kera;
import dev.kamest.kera.alg.AbstractKeraTest;
import dev.kamest.kera.alg.KeraComputer;
import dev.kamest.kera.model.KeraOpinion;
import dev.kamest.kera.model.KeraResultItem;
import dev.kamest.kera.prov.ext.OpinionsProvider;
import lombok.extern.apachecommons.CommonsLog;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Just see test - data are manually computed or taken from Grouplens tests.
 *
 * Thanks https://grouplens.org/ for providing incredible datasets.
 * <p>
 * This code is part of bachelor thesis:
 * Application of machine learning algorithms on purchase recommendation
 * UHK FIM KIT
 *
 * @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 **/
@CommonsLog
public class TFIDFComputerTest extends AbstractKeraTest {

    @Override
    protected KeraComputer getComputer(Kera kera) {
        return new TFIDFComputer(isRanking,true);
    }

    @Override
    protected boolean needsRegisteredUsers() {
        return TFIDFComputer.needsRegisteredUsers();
    }

    @Override
    protected String getComputerCode() {
        return "TFIDFTest";
    }

    @Test
    public void testComputer(){
        initGlData();
        OpinionsProvider opinionsProvider = getOpinionsProvider();
        Kera kera = new Kera(getItemsProvider(),getUsersProvider(needsRegisteredUsers()), opinionsProvider);
        if (isComputeNewModel()) log.info("start training");
        TFIDFComputer computer = new TFIDFComputer(isRanking,true);
        if (isComputeNewModel()){
            kera.addComputerWithComputation("TFIDFTest",computer);
        }else {
            kera.addComputer("TFIDFTest",computer);
        }
        log.info("start recommending");

        long recStart = System.currentTimeMillis();
        List<KeraResultItem> resultItems = computer.recommend(
                "320",
                10,
                Collections.emptyList(),
                true,
                false);
        log.info("Recommendation took: " + (System.currentTimeMillis() - recStart) + "ms");
        Assert.assertEquals("32",resultItems.get(0).getExternalId());
        Assert.assertEquals(5.0,resultItems.get(0).getScore(),0.01);
        Assert.assertEquals("1748",resultItems.get(1).getExternalId());
        Assert.assertEquals(4.3906,resultItems.get(1).getScore(),0.01);
        Assert.assertEquals("1206",resultItems.get(2).getExternalId());
        Assert.assertEquals(4.2656,resultItems.get(2).getScore(),0.01);
        Assert.assertEquals("48394",resultItems.get(3).getExternalId());
        Assert.assertEquals(4.1293,resultItems.get(3).getScore(),0.01);
        Assert.assertEquals("1199",resultItems.get(4).getExternalId());
        Assert.assertEquals( 4.1186,resultItems.get(4).getScore(),0.01);
        Assert.assertEquals("32587",resultItems.get(5).getExternalId());
        Assert.assertEquals( 4.1146,resultItems.get(5).getScore(),0.01);
        Assert.assertEquals("1270", resultItems.get(6).getExternalId());
        Assert.assertEquals( 4.0571,resultItems.get(6).getScore(),0.01);
        Assert.assertEquals("1089", resultItems.get(7).getExternalId());
        Assert.assertEquals( 3.9826,resultItems.get(7).getScore(),0.01);
        Assert.assertEquals("1210",resultItems.get(8).getExternalId());
        Assert.assertEquals(3.9644,resultItems.get(8).getScore(),0.01);
        Assert.assertEquals("7147",resultItems.get(9).getExternalId());
        Assert.assertEquals(3.7805,resultItems.get(9).getScore(),0.01);


        recStart = System.currentTimeMillis();
        resultItems = kera.recommend(
                "TFIDFTest",
                "320",
                10,
                true);
        log.info("Recommendation took: " + (System.currentTimeMillis() - recStart) + "ms");

        Assert.assertEquals("48394",resultItems.get(0).getExternalId());
        Assert.assertEquals(5.0,resultItems.get(0).getScore(),0.01);
        Assert.assertEquals("7153",resultItems.get(1).getExternalId());
        Assert.assertEquals(4.6698,resultItems.get(1).getScore(),0.01);
        Assert.assertEquals("1210",resultItems.get(2).getExternalId());
        Assert.assertEquals(4.5653,resultItems.get(2).getScore(),0.01);
        Assert.assertEquals("628",resultItems.get(3).getExternalId());
        Assert.assertEquals(4.5538,resultItems.get(3).getScore(),0.01);
        Assert.assertEquals("2997",resultItems.get(4).getExternalId());
        Assert.assertEquals( 4.5287,resultItems.get(4).getScore(),0.01);
        Assert.assertEquals("1089",resultItems.get(5).getExternalId());
        Assert.assertEquals( 4.3483,resultItems.get(5).getScore(),0.01);
        Assert.assertEquals("7147", resultItems.get(6).getExternalId());
        Assert.assertEquals( 4.2824,resultItems.get(6).getScore(),0.01);
        Assert.assertEquals("32587", resultItems.get(7).getExternalId());
        Assert.assertEquals( 4.2675,resultItems.get(7).getScore(),0.01);
        Assert.assertEquals("48780",resultItems.get(8).getExternalId());
        Assert.assertEquals(4.2563,resultItems.get(8).getScore(),0.01);
        Assert.assertEquals("1199",resultItems.get(9).getExternalId());
        Assert.assertEquals(4.1703,resultItems.get(9).getScore(),0.01);
    }

    @Test
    public void testComputerSnData(){
        initSNData();
        String computerCode = "TFIDFTestSn";

        OpinionsProvider opinionsProvider = getOpinionsProvider();
        Map<String,List<KeraOpinion>> ops = new HashMap<>();

        opinionsProvider.getObjects().forEach(i->{
            List<KeraOpinion> o = ops.getOrDefault(i.getUserId(),new LinkedList<>());
            o.add(i);
            ops.put(i.getUserId(), o);
        });

        Kera kera = new Kera(getItemsProvider(),getUsersProvider(needsRegisteredUsers()), opinionsProvider);
        if (isComputeNewModel()) log.info("start training");
        TFIDFComputer computer = new TFIDFComputer(isRanking,true);
        if (isComputeNewModel()){
            kera.addComputerWithComputation(computerCode,computer);
        }else {
            kera.addComputer(computerCode,computer);
        }
        log.info("start recommending");

        long recStart = System.currentTimeMillis();
        List<KeraResultItem> resultItems = computer.recommend(
                "ffc64d83-db43-480e-ab80-80356f5fce01",
                10,
                Collections.emptyList(),
                true,
                false);
        log.info("Recommendation took: " + (System.currentTimeMillis() - recStart) + "ms");

        Assert.assertEquals("055-0025-cis-barva-best-1-v-olse-best1",resultItems.get(0).getExternalId());
        Assert.assertEquals(5.0,resultItems.get(0).getScore(),0.01);
        Assert.assertEquals("055-0013-cis-barva-best-1-v-olse-best1",resultItems.get(1).getExternalId());
        Assert.assertEquals(5.0,resultItems.get(1).getScore(),0.01);
        Assert.assertEquals("055-0020-cis-barva-best-1-v-olse-best1",resultItems.get(2).getExternalId());
        Assert.assertEquals(5.0,resultItems.get(2).getScore(),0.01);
        Assert.assertEquals("055-0018-cis-barva-best-1-v-olse-best1",resultItems.get(3).getExternalId());
        Assert.assertEquals(5.0,resultItems.get(3).getScore(),0.01);
        Assert.assertEquals("055-0001-cis-barva-best-1-v-olse-best1",resultItems.get(4).getExternalId());
        Assert.assertEquals( 5.0,resultItems.get(4).getScore(),0.01);
        Assert.assertEquals("055-0015-cis-barva-best-1-v-olse-best1",resultItems.get(5).getExternalId());
        Assert.assertEquals( 5.0,resultItems.get(5).getScore(),0.01);

    }

    @Test
    public void testComputerProData(){
        initProData();
        String computerCode = "TFIDFTestPro";

        OpinionsProvider opinionsProvider = getOpinionsProvider();
        Kera kera = new Kera(getItemsProvider(),getUsersProvider(needsRegisteredUsers()), opinionsProvider);
        if (isComputeNewModel()) log.info("start training");
        TFIDFComputer computer = new TFIDFComputer(isRanking,true);
        if (isComputeNewModel()){
            kera.addComputerWithComputation(computerCode,computer);
        }else {
            kera.addComputer(computerCode,computer);
        }
        log.info("start recommending");

        long recStart = System.currentTimeMillis();
        List<KeraResultItem> resultItems = computer.recommend(
                "d5da48a4-91e3-4be0-9f6f-504686be372d",
                10,
                Collections.emptyList(),
                true,
                false);
        log.info("Recommendation took: " + (System.currentTimeMillis() - recStart) + "ms");

        Assert.assertEquals("71202E95-E88F-4CA1-92B9-6AD318230187",resultItems.get(0).getExternalId());
        Assert.assertEquals(5.0,resultItems.get(0).getScore(),0.01);
        Assert.assertEquals("863E702E-73B0-4177-B5BF-6FB3D8A1EFF4",resultItems.get(1).getExternalId());
        Assert.assertEquals(5.0,resultItems.get(1).getScore(),0.01);
        Assert.assertEquals("02A729B3-1A0A-4CA7-81E1-0D05821F7DFE",resultItems.get(2).getExternalId());
        Assert.assertEquals(5.0,resultItems.get(2).getScore(),0.01);
        Assert.assertEquals("267F3B1C-687B-4A98-8AB8-AE41467CB3F1",resultItems.get(3).getExternalId());
        Assert.assertEquals(5.0,resultItems.get(3).getScore(),0.01);
        Assert.assertEquals("501BA407-6C7C-4609-A49A-2877BF0052AF",resultItems.get(4).getExternalId());
        Assert.assertEquals( 5.0,resultItems.get(4).getScore(),0.01);
        Assert.assertEquals("12D8CADD-1D3C-43C9-AF91-2F526246C7DC",resultItems.get(5).getExternalId());
        Assert.assertEquals( 5.0,resultItems.get(5).getScore(),0.01);
        Assert.assertEquals("D5EF2D71-455A-495A-A615-1644A56A7CB1", resultItems.get(6).getExternalId());
        Assert.assertEquals( 5.0,resultItems.get(6).getScore(),0.01);
        Assert.assertEquals("2CC5B332-2BD5-481B-9668-C80AAAE34EE1", resultItems.get(7).getExternalId());
        Assert.assertEquals( 5.0,resultItems.get(7).getScore(),0.01);
        Assert.assertEquals("78F27519-9A4B-4A47-B776-C8122AA6E5E5",resultItems.get(8).getExternalId());
        Assert.assertEquals(5.0,resultItems.get(8).getScore(),0.01);
        Assert.assertEquals("CE9F77F2-F8A2-4466-A27B-EDE71E623DB1",resultItems.get(9).getExternalId());
        Assert.assertEquals(5.0,resultItems.get(9).getScore(),0.01);

    }
}
