package dev.kamest.kera.alg.nn;

import dev.kamest.kera.Kera;
import dev.kamest.kera.alg.AbstractKeraTest;
import dev.kamest.kera.alg.KeraComputer;
import dev.kamest.kera.model.KeraItem;
import dev.kamest.kera.model.KeraOpinion;
import dev.kamest.kera.model.KeraResultItem;
import dev.kamest.kera.prov.ext.OpinionsProvider;
import lombok.extern.apachecommons.CommonsLog;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Just see test - data are manually computed or taken from Grouplens tests.
 *
 * Thanks https://grouplens.org/ for providing incredible datasets.
 * <p>
 * This code is part of bachelor thesis:
 * Application of machine learning algorithms on purchase recommendation
 * UHK FIM KIT
 *
 * @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 **/
@CommonsLog
public class ItemItemNNComputerTest extends AbstractKeraTest {

    @Override
    protected KeraComputer getComputer(Kera kera) {
        ItemItemNNComputer computer = new ItemItemNNComputer(isRanking);
        computer.setNeighborhoodSize(20);
        return computer;
    }

    @Override
    protected String getComputerCode() {
        return "IINNTest";
    }

    @Override
    protected boolean needsRegisteredUsers() {
        return ItemItemNNComputer.needsRegisteredUsers();
    }
    @Test
    public void testComputer(){
        initGlData();
        OpinionsProvider opinionsProvider = getOpinionsProvider();
        Kera kera = new Kera(getItemsProvider(),getUsersProvider(needsRegisteredUsers()), opinionsProvider);
        if (isComputeNewModel()) log.info("start training");
        ItemItemNNComputer computer = new ItemItemNNComputer(isRanking);
        computer.setNeighborhoodSize(20);
        if (isComputeNewModel()){
            kera.addComputerWithComputation("IINNTest",computer);
        }else {
            kera.addComputer("IINNTest",computer);
        }
        log.info("start recommending");
        long recStart = System.currentTimeMillis();
        List<KeraResultItem> resultItems = kera.recommend(
                "IINNTest",
                "320",
                10,
                Arrays.asList(new KeraItem("153"), new KeraItem("260"), new KeraItem("527"), new KeraItem("588")),
                true);

        log.info("Recommendation took: " + (System.currentTimeMillis() - recStart) + "ms");

        Assert.assertEquals("1210", resultItems.get(0).getExternalId());
        Assert.assertEquals(1.099d, resultItems.get(0).getScore(), 0.01);
        Assert.assertEquals("364", resultItems.get(1).getExternalId());
        Assert.assertEquals(1.012d, resultItems.get(1).getScore(), 0.01);
        Assert.assertEquals("595", resultItems.get(2).getExternalId());
        Assert.assertEquals(1.005d, resultItems.get(2).getScore(), 0.01);
        Assert.assertEquals("1", resultItems.get(3).getExternalId());
        Assert.assertEquals(0.925d, resultItems.get(3).getScore(), 0.01);
        Assert.assertEquals("500", resultItems.get(4).getExternalId());
        Assert.assertEquals(0.893d, resultItems.get(4).getScore(), 0.01);
        Assert.assertEquals("5349", resultItems.get(5).getExternalId());
        Assert.assertEquals(0.891d, resultItems.get(5).getScore(), 0.01);
        Assert.assertEquals("480", resultItems.get(6).getExternalId());
        Assert.assertEquals(0.888d, resultItems.get(6).getScore(), 0.01);
        Assert.assertEquals("1291", resultItems.get(7).getExternalId());
        Assert.assertEquals(0.885d, resultItems.get(7).getScore(), 0.01);
        Assert.assertEquals("150", resultItems.get(8).getExternalId());
        Assert.assertEquals(0.871d, resultItems.get(8).getScore(), 0.01);


        recStart = System.currentTimeMillis();
        resultItems = kera.recommend(
                "IINNTest",
                "320",
                10, true);
        log.info("Recommendation took: " + (System.currentTimeMillis() - recStart) + "ms");

        Assert.assertEquals("7502", resultItems.get(0).getExternalId());
        Assert.assertEquals(4.484d,resultItems.get(0).getScore(),0.01);
        Assert.assertEquals("1224",resultItems.get(1).getExternalId());
        Assert.assertEquals(4.423d,resultItems.get(1).getScore(),0.01);
        Assert.assertEquals("858",resultItems.get(2).getExternalId());
        Assert.assertEquals(4.408d,resultItems.get(2).getScore(),0.01);
        Assert.assertEquals("318",resultItems.get(3).getExternalId());
        Assert.assertEquals(4.403d,resultItems.get(3).getScore(),0.01);
        Assert.assertEquals("1203",resultItems.get(4).getExternalId());
        Assert.assertEquals( 4.386d,resultItems.get(4).getScore(),0.01);
        Assert.assertEquals("3462",resultItems.get(5).getExternalId());
        Assert.assertEquals( 4.379d,resultItems.get(5).getScore(),0.01);
        Assert.assertTrue("99114".equals(resultItems.get(6).getExternalId()) || "4973".equals(resultItems.get(6).getExternalId()));
        Assert.assertEquals( 4.376d,resultItems.get(6).getScore(),0.01);
        Assert.assertTrue("99114".equals(resultItems.get(7).getExternalId()) || "4973".equals(resultItems.get(7).getExternalId()));
        Assert.assertEquals( 4.376d,resultItems.get(7).getScore(),0.01);
        Assert.assertEquals("898",resultItems.get(8).getExternalId());
        Assert.assertEquals(4.371d,resultItems.get(8).getScore(),0.01);
    }


    @Test
    public void testComputerSnData(){
        initSNData();
        String computerCode = "IINNTestSn";

        OpinionsProvider opinionsProvider = getOpinionsProvider();
        Map<String,List<KeraOpinion>> ops = new HashMap<>();

        opinionsProvider.getObjects().forEach(i->{
            List<KeraOpinion> o = ops.getOrDefault(i.getUserId(),new LinkedList<>());
            o.add(i);
            ops.put(i.getUserId(), o);
        });

        Kera kera = new Kera(getItemsProvider(),getUsersProvider(needsRegisteredUsers()), opinionsProvider);
        if (isComputeNewModel()) log.info("start training");
        KeraComputer computer = new ItemItemNNComputer(isRanking);
        computer.setNeighborhoodSize(100);
        if (isComputeNewModel()){
            kera.addComputerWithComputation(computerCode,computer);
        }else {
            kera.addComputer(computerCode,computer);
        }
        log.info("start recommending");
        long recStart = System.currentTimeMillis();
        List<KeraResultItem> resultItems = kera.recommend(
                computerCode,
                "ffc64d83-db43-480e-ab80-80356f5fce01",
                10,
                true);

        log.info("Recommendation took: " + (System.currentTimeMillis() - recStart) + "ms" );

        Assert.assertEquals("055-0013-cis-barva-best-1-v-olse-best1",resultItems.get(0).getExternalId());
        Assert.assertEquals(0.18221847955683437,resultItems.get(0).getScore(),0.01);
        Assert.assertEquals("055-0014-cis-barva-best-1-v-olse-best1",resultItems.get(1).getExternalId());
        Assert.assertEquals(0.17423475515307904,resultItems.get(1).getScore(),0.01);
        Assert.assertEquals("055-0001-cis-barva-best-1-v-olse-best1",resultItems.get(2).getExternalId());
        Assert.assertEquals(0.16832516425495048,resultItems.get(2).getScore(),0.01);
        Assert.assertEquals("055-0011-cis-barva-best-1-v-olse-best1", resultItems.get(3).getExternalId());
        Assert.assertEquals(0.16163359214578393,resultItems.get(3).getScore(),0.01);
        Assert.assertEquals("055-0015-cis-barva-best-1-v-olse-best1", resultItems.get(4).getExternalId());
        Assert.assertEquals(0.1577295653880696,resultItems.get(4).getScore(),0.01);
        Assert.assertEquals("031-0059-cis-barva-dreva-magnat-v-olse-magnat", resultItems.get(5).getExternalId());
        Assert.assertEquals(0.15324232634020074,resultItems.get(5).getScore(),0.01);

    }


    @Test
    public void testComputerProData(){
        initProData();
        String computerCode = "IINNTestPro";
        OpinionsProvider opinionsProvider = getOpinionsProvider();

        Map<String,List<KeraOpinion>> ops = new HashMap<>();

        opinionsProvider.getObjects().forEach(i->{
            List<KeraOpinion> o = ops.getOrDefault(i.getUserId(),new LinkedList<>());
            o.add(i);
            ops.put(i.getUserId(), o);
        });

        Kera kera = new Kera(getItemsProvider(),getUsersProvider(needsRegisteredUsers()), opinionsProvider);
        if (isComputeNewModel()) log.info("start training");
        KeraComputer computer = new ItemItemNNComputer(isRanking);
        computer.setNeighborhoodSize(100);
        if (isComputeNewModel()){
            kera.addComputerWithComputation(computerCode,computer);
        }else {
            kera.addComputer(computerCode,computer);
        }
        log.info("start recommending");
        long recStart = System.currentTimeMillis();
        List<KeraResultItem> resultItems = kera.recommend(
                computerCode,
                "d5da48a4-91e3-4be0-9f6f-504686be372d",
                10,
                true);

        log.info("Recommendation took: " + (System.currentTimeMillis() - recStart) + "ms" );

        Assert.assertEquals("9C0FF489-1DB9-4104-8103-C140CFD11824",resultItems.get(0).getExternalId());
        Assert.assertEquals(6.6423123879843,resultItems.get(0).getScore(),0.01);
        Assert.assertEquals("5D25E490-4C32-40FC-905B-D5F6ACBB9ECF",resultItems.get(1).getExternalId());
        Assert.assertEquals(6.372952686025466,resultItems.get(1).getScore(),0.01);
        Assert.assertEquals("FA87F7CF-68CB-4405-B26E-3CED719E4E7E",resultItems.get(2).getExternalId());
        Assert.assertEquals(5.732851578227566,resultItems.get(2).getScore(),0.01);
        Assert.assertEquals("C8829DF7-F4FB-4FF2-91B0-ED60F10470AA", resultItems.get(3).getExternalId());
        Assert.assertEquals(5.629801443505732,resultItems.get(3).getScore(),0.01);
        Assert.assertEquals("6E8ED07B-F0C0-4008-AD27-9997D843EC67", resultItems.get(4).getExternalId());
        Assert.assertEquals(5.322500886893039,resultItems.get(4).getScore(),0.01);
        Assert.assertEquals("6206031D-41A2-454D-8C07-BCAA3DBF6DE4", resultItems.get(5).getExternalId());
        Assert.assertEquals(5.297836816583043,resultItems.get(5).getScore(),0.01);

    }

}
