package dev.kamest.kera.alg.nn;

import dev.kamest.kera.Kera;
import dev.kamest.kera.alg.AbstractKeraTest;
import dev.kamest.kera.alg.KeraComputer;
import dev.kamest.kera.alg.KeraComputer;
import dev.kamest.kera.model.KeraItem;
import dev.kamest.kera.model.KeraOpinion;
import dev.kamest.kera.model.KeraResultItem;
import dev.kamest.kera.prov.ext.OpinionsProvider;
import lombok.extern.apachecommons.CommonsLog;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Just see test - data are manually computed or taken from Grouplens tests.
 *
 * Thanks https://grouplens.org/ for providing incredible datasets.
 * <p>
 * This code is part of bachelor thesis:
 * Application of machine learning algorithms on purchase recommendation
 * UHK FIM KIT
 *
 * @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 **/
@CommonsLog
public class UserUserNNComputerTest extends AbstractKeraTest {

    @Override
    protected KeraComputer getComputer(Kera kera) {
        return new UserUserNNComputer(isRanking);
    }

    @Override
    protected String getComputerCode() {
        return "UUNNTest";
    }

    @Override
    protected boolean needsRegisteredUsers() {
        return UserUserNNComputer.needsRegisteredUsers();
    }
    @Test
    public void testComputer(){
        initGlData();
        OpinionsProvider opinionsProvider = getOpinionsProvider();
        Kera kera = new Kera(getItemsProvider(),getUsersProvider(needsRegisteredUsers()), opinionsProvider);

        if (isComputeNewModel()) log.info("start training");
        KeraComputer computer = new UserUserNNComputer(false);
//        if (isComputeNewModel()){
//            kera.addComputerWithComputation("UUNNTest",computer);
//        }else {
            kera.addComputer("UUNNTest",computer);
//        }
        log.info("start recommending");
        long recStart = System.currentTimeMillis();
        List<KeraResultItem> resultItems = kera.recommend(
                "UUNNTest",
                "320",
                10000,
                false);

        log.info("Recommendation took: " + (System.currentTimeMillis() - recStart) + "ms");
        Assert.assertEquals(2.745,resultItems.stream().filter(i->i.getExternalId().equals("153")).findFirst().orElse(new KeraResultItem(new KeraItem("0"),0.0)).getScore(),0.01);
        Assert.assertEquals(4.480,resultItems.stream().filter(i->i.getExternalId().equals("260")).findFirst().orElse(new KeraResultItem(new KeraItem("0"),0.0)).getScore(),0.01);
        Assert.assertEquals(4.315,resultItems.stream().filter(i->i.getExternalId().equals("527")).findFirst().orElse(new KeraResultItem(new KeraItem("0"),0.0)).getScore(),0.01);
        Assert.assertEquals(3.676,resultItems.stream().filter(i->i.getExternalId().equals("588")).findFirst().orElse(new KeraResultItem(new KeraItem("0"),0.0)).getScore(),0.01);

        recStart = System.currentTimeMillis();
        resultItems = kera.recommend(
                "UUNNTest",
                "320",
                10,
                true);
        log.info("Recommendation took: " + (System.currentTimeMillis() - recStart) + "ms");

        Assert.assertEquals("318",resultItems.get(0).getExternalId());
        Assert.assertEquals(4.590566994735333,resultItems.get(0).getScore(),0.01);
        Assert.assertEquals("858", resultItems.get(1).getExternalId());
        Assert.assertEquals(4.5081331132672835,resultItems.get(1).getScore(),0.01);
        Assert.assertEquals("2859", resultItems.get(2).getExternalId());
        Assert.assertEquals(4.4857514249203705,resultItems.get(2).getScore(),0.01);
        Assert.assertEquals("4973",resultItems.get(3).getExternalId());
        Assert.assertEquals(4.46032428992179,resultItems.get(3).getScore(),0.01);
        Assert.assertEquals("302",resultItems.get(4).getExternalId());
        Assert.assertEquals( 4.4595273064197904,resultItems.get(4).getScore(),0.01);
        Assert.assertEquals("2360",resultItems.get(5).getExternalId());
        Assert.assertEquals( 4.450816693922738,resultItems.get(5).getScore(),0.01);
        Assert.assertEquals("2019", resultItems.get(6).getExternalId());
        Assert.assertEquals( 4.435629518018265,resultItems.get(6).getScore(),0.01);
        Assert.assertEquals("5971", resultItems.get(7).getExternalId());
        Assert.assertEquals( 4.429012611303996,resultItems.get(7).getScore(),0.01);
        Assert.assertEquals("1203",resultItems.get(8).getExternalId());
        Assert.assertEquals( 4.415622687607228,resultItems.get(8).getScore(),0.01);
    }


    @Test
    public void testComputerSnData(){
        initSNData();
        String computerCode = "UUNNTestSn";

        OpinionsProvider opinionsProvider = getOpinionsProvider();
        Map<String,List<KeraOpinion>> ops = new HashMap<>();

        opinionsProvider.getObjects().forEach(i->{
            List<KeraOpinion> o = ops.getOrDefault(i.getUserId(),new LinkedList<>());
            o.add(i);
            ops.put(i.getUserId(), o);
        });

        Kera kera = new Kera(getItemsProvider(),getUsersProvider(needsRegisteredUsers()), opinionsProvider);
        if (isComputeNewModel()) log.info("start training");
        KeraComputer computer = new UserUserNNComputer(isRanking);
        computer.setNeighborhoodSize(20);
        if (isComputeNewModel()){
            kera.addComputerWithComputation(computerCode,computer);
        }else {
            kera.addComputer(computerCode,computer);
        }
        log.info("start recommending");
        long recStart = System.currentTimeMillis();
        List<KeraResultItem> resultItems = kera.recommend(
                computerCode,
                "ffc64d83-db43-480e-ab80-80356f5fce01",
                10,
                true);

        log.info("Recommendation took: " + (System.currentTimeMillis() - recStart) + "ms" );

        Assert.assertEquals("055-0015-cis-barva-best-1-v-olse-best1",resultItems.get(0).getExternalId());
        Assert.assertEquals(1.3653550716504106,resultItems.get(0).getScore(),0.01);
        Assert.assertEquals("055-0011-cis-barva-best-1-v-olse-best1",resultItems.get(1).getExternalId());
        Assert.assertEquals(1.0693448094268978,resultItems.get(1).getScore(),0.01);
        Assert.assertEquals("055-0012-cis-barva-best-1-v-olse-best1",resultItems.get(2).getExternalId());
        Assert.assertEquals(0.8922285251880866,resultItems.get(2).getScore(),0.01);
        Assert.assertEquals("055-0013-cis-barva-best-1-v-wenge-best1", resultItems.get(3).getExternalId());
        Assert.assertEquals(0.8838834764831843,resultItems.get(3).getScore(),0.01);
        Assert.assertEquals("002-0496-cis-barva-dreva-maxidrew-v-ols-maxidrew", resultItems.get(4).getExternalId());
        Assert.assertEquals(0.8606504100245023,resultItems.get(4).getScore(),0.01);
        Assert.assertEquals("058-0103", resultItems.get(5).getExternalId());
        Assert.assertEquals(0.8535533905932737,resultItems.get(5).getScore(),0.01);

    }

    @Test
    public void testComputerProData(){
        initProData();
        String computerCode = "UUNNTestPro";

        OpinionsProvider opinionsProvider = getOpinionsProvider();
        Kera kera = new Kera(getItemsProvider(),getUsersProvider(needsRegisteredUsers()), opinionsProvider);
        if (isComputeNewModel()) log.info("start training");
        KeraComputer computer = new UserUserNNComputer(isRanking);
        computer.setNeighborhoodSize(20);
        if (isComputeNewModel()){
            kera.addComputerWithComputation(computerCode,computer);
        }else {
            kera.addComputer(computerCode,computer);
        }
        log.info("start recommending");
        long recStart = System.currentTimeMillis();
        List<KeraResultItem> resultItems = kera.recommend(
                computerCode,
                "d5da48a4-91e3-4be0-9f6f-504686be372d",
                10,
                true);

        log.info("Recommendation took: " + (System.currentTimeMillis() - recStart) + "ms" );

        Assert.assertEquals("9C0FF489-1DB9-4104-8103-C140CFD11824",resultItems.get(0).getExternalId());
        Assert.assertEquals(5.009412026275384,resultItems.get(0).getScore(),0.01);
        Assert.assertEquals("C8829DF7-F4FB-4FF2-91B0-ED60F10470AA",resultItems.get(1).getExternalId());
        Assert.assertEquals(4.85434024046579,resultItems.get(1).getScore(),0.01);
        Assert.assertEquals("5D25E490-4C32-40FC-905B-D5F6ACBB9ECF",resultItems.get(2).getExternalId());
        Assert.assertEquals(4.697356515569535,resultItems.get(2).getScore(),0.01);
        Assert.assertEquals("60510331-7F9C-4001-8E55-D9F39A8B3526", resultItems.get(3).getExternalId());
        Assert.assertEquals(4.582236961342192,resultItems.get(3).getScore(),0.01);
        Assert.assertEquals("4567F104-7088-4954-A443-34D318780D60", resultItems.get(4).getExternalId());
        Assert.assertEquals(4.572476372701311,resultItems.get(4).getScore(),0.01);
        Assert.assertEquals("CE8538B6-DC9D-4B71-8B2A-07F5028085E1", resultItems.get(5).getExternalId());
        Assert.assertEquals(4.550872245908279,resultItems.get(5).getScore(),0.01);

    }

}
