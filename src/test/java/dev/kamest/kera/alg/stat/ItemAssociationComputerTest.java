package dev.kamest.kera.alg.stat;

import dev.kamest.kera.Kera;
import dev.kamest.kera.alg.AbstractKeraTest;
import dev.kamest.kera.alg.KeraComputer;
import dev.kamest.kera.alg.nn.UserUserNNComputer;
import dev.kamest.kera.model.KeraItem;
import dev.kamest.kera.model.KeraOpinion;
import dev.kamest.kera.model.KeraResultItem;
import dev.kamest.kera.prov.ext.OpinionsProvider;
import lombok.extern.apachecommons.CommonsLog;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Just see test - data are manually computed or taken from Grouplens tests.
 *
 * Thanks https://grouplens.org/ for providing incredible datasets.
 * <p>
 * This code is part of bachelor thesis:
 * Application of machine learning algorithms on purchase recommendation
 * UHK FIM KIT
 *
 * @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 **/
@CommonsLog
public class ItemAssociationComputerTest extends AbstractKeraTest {

    @Override
    protected KeraComputer getComputer(Kera kera) {
        return new ItemAssociationComputer(isRanking);
    }

    @Override
    protected String getComputerCode() {
        return "IATest";
    }

    @Override
    protected boolean needsRegisteredUsers() {
        return ItemAssociationComputer.needsRegisteredUsers();
    }
    @Test
    public void testComputer(){
        initGlData();
        OpinionsProvider opinionsProvider = getOpinionsProvider();
        Kera kera = new Kera(getItemsProvider(),getUsersProvider(needsRegisteredUsers()), opinionsProvider);
        if (isComputeNewModel()) log.info("start training");
        ItemAssociationComputer computer = new ItemAssociationComputer(false);
        if (isComputeNewModel()){
            kera.addComputerWithComputation("IATest",computer);
        }else {
            kera.addComputer("IATest",computer);
        }

        log.info("start recommending");

        Map<Integer, Double> testItemVec = computer.getModel().get(computer.getItemId("260"));
        Assert.assertEquals(0.916d,testItemVec.get(computer.getItemId("2571")),0.01d);
        Assert.assertEquals(0.899d,testItemVec.get(computer.getItemId("1196")),0.01d);
        Assert.assertEquals(0.892d,testItemVec.get(computer.getItemId("4993")),0.01d);

        long recStart = System.currentTimeMillis();
        List<KeraResultItem> resultItems = kera.recommend("IATest", "90097", 5,true);
        log.info("Recommendation took: " + (System.currentTimeMillis() - recStart) + "ms");
        Assert.assertEquals("2802",resultItems.get(0).getExternalId());
        Assert.assertEquals(5.0,resultItems.get(0).getScore(),0.01);
        Assert.assertEquals("2861",resultItems.get(1).getExternalId());
        Assert.assertEquals(4.79638,resultItems.get(1).getScore(),0.01);
        Assert.assertEquals("1401",resultItems.get(2).getExternalId());
        Assert.assertEquals(4.78951,resultItems.get(2).getScore(),0.01);

    }
    @Test
    public void testComputerSnData(){
        initSNData();
        String computerCode = "IATestSn";

        OpinionsProvider opinionsProvider = getOpinionsProvider();
        Map<String,List<KeraOpinion>> ops = new HashMap<>();

        opinionsProvider.getObjects().forEach(i->{
            List<KeraOpinion> o = ops.getOrDefault(i.getUserId(),new LinkedList<>());
            o.add(i);
            ops.put(i.getUserId(), o);
        });

        Kera kera = new Kera(getItemsProvider(),getUsersProvider(needsRegisteredUsers()), opinionsProvider);
        if (isComputeNewModel()) log.info("start training");
        KeraComputer computer = new ItemAssociationComputer(isRanking);
        computer.setNeighborhoodSize(20);
        if (isComputeNewModel()){
            kera.addComputerWithComputation(computerCode,computer);
        }else {
            kera.addComputer(computerCode,computer);
        }
        log.info("start recommending");
        long recStart = System.currentTimeMillis();
        List<KeraResultItem> resultItems = kera.recommend(
                computerCode,
                "ffc64d83-db43-480e-ab80-80356f5fce01",
                10,
                true);

        log.info("Recommendation took: " + (System.currentTimeMillis() - recStart) + "ms" );

        Assert.assertEquals("012-0555-cis-stul-drew12-v-rustikal-drew",resultItems.get(0).getExternalId());
        Assert.assertEquals(5.0,resultItems.get(0).getScore(),0.01);
        Assert.assertEquals("012-0503-cis-barva-drew3-v-rustikal-drew-cis-latka-drew-v-11-drew",resultItems.get(1).getExternalId());
        Assert.assertEquals(5.0,resultItems.get(1).getScore(),0.01);
        Assert.assertEquals("115-0170",resultItems.get(2).getExternalId());
        Assert.assertEquals(2.5,resultItems.get(2).getScore(),0.01);
        Assert.assertEquals("006-0135-cis-barva-ram-bms-v-bila-bms-100", resultItems.get(3).getExternalId());
        Assert.assertEquals(2.5,resultItems.get(3).getScore(),0.01);
        Assert.assertEquals("065-0137-cis-barva-imperia-v-seda-imperia", resultItems.get(4).getExternalId());
        Assert.assertEquals(2.5,resultItems.get(4).getScore(),0.01);
        Assert.assertEquals("010-0029-cis-b-sedacka-valerie3-v-seda-s-v-cis-barva-pols-valerie3-v-tmmodra-p-v", resultItems.get(5).getExternalId());
        Assert.assertEquals(2.5,resultItems.get(5).getScore(),0.01);

    }
    @Test
    public void testComputerProData(){
        initProData();
        String computerCode = "IATestPro";

        OpinionsProvider opinionsProvider = getOpinionsProvider();
        Map<String,List<KeraOpinion>> ops = new HashMap<>();

        opinionsProvider.getObjects().forEach(i->{
            List<KeraOpinion> o = ops.getOrDefault(i.getUserId(),new LinkedList<>());
            o.add(i);
            ops.put(i.getUserId(), o);
        });


        Kera kera = new Kera(getItemsProvider(),getUsersProvider(needsRegisteredUsers()), opinionsProvider);
        if (isComputeNewModel()) log.info("start training");
        KeraComputer computer = new ItemAssociationComputer(isRanking);
        computer.setNeighborhoodSize(20);
        if (isComputeNewModel()){
            kera.addComputerWithComputation(computerCode,computer);
        }else {
            kera.addComputer(computerCode,computer);
        }
        log.info("start recommending");
        long recStart = System.currentTimeMillis();
        List<KeraResultItem> resultItems = kera.recommend(
                computerCode,
                "d5da48a4-91e3-4be0-9f6f-504686be372d",
                10,
                true);

        log.info("Recommendation took: " + (System.currentTimeMillis() - recStart) + "ms" );

        Assert.assertEquals("4DA32897-3172-428F-AE73-003F67F9446A",resultItems.get(0).getExternalId());
        Assert.assertEquals(5.0,resultItems.get(0).getScore(),0.01);
        Assert.assertEquals("B0B8C347-3C44-4684-8AE5-E37653DCB066",resultItems.get(1).getExternalId());
        Assert.assertEquals(3.75,resultItems.get(1).getScore(),0.01);
        Assert.assertEquals("6A2137CA-27A0-4F36-A47A-E25305DE4CEB",resultItems.get(2).getExternalId());
        Assert.assertEquals(3.75,resultItems.get(2).getScore(),0.01);
        Assert.assertEquals("F5427DA5-7280-483F-B0B4-B28E175D9DBC", resultItems.get(3).getExternalId());
        Assert.assertEquals(3.125,resultItems.get(3).getScore(),0.01);
        Assert.assertEquals("EBD2AC8A-5073-4675-B099-AFB6257A0EE3", resultItems.get(4).getExternalId());
        Assert.assertEquals(3.125,resultItems.get(4).getScore(),0.01);
        Assert.assertEquals("2F1A39CE-CF88-4E9A-8540-6BDF3A955938", resultItems.get(5).getExternalId());
        Assert.assertEquals(3.125,resultItems.get(5).getScore(),0.01);

    }

}
