package dev.kamest.kera.alg.stat;

import dev.kamest.kera.Kera;
import dev.kamest.kera.alg.AbstractKeraTest;
import dev.kamest.kera.alg.KeraComputer;
import dev.kamest.kera.model.KeraOpinion;
import dev.kamest.kera.model.KeraResultItem;
import dev.kamest.kera.prov.ext.OpinionsProvider;
import lombok.extern.apachecommons.CommonsLog;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Just see test - data are manually computed or taken from Grouplens tests.
 *
 * Thanks https://grouplens.org/ for providing incredible datasets.
 * <p>
 * This code is part of bachelor thesis:
 * Application of machine learning algorithms on purchase recommendation
 * UHK FIM KIT
 *
 * @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 **/
@CommonsLog
public class ItemMeanPopularityComputerTest extends AbstractKeraTest {

    @Override
    protected KeraComputer getComputer(Kera kera) {
        return new ItemMeanPopularityComputer(isRanking);
    }

    @Override
    protected String getComputerCode() {
        return "IMPCTest";
    }

    @Override
    protected boolean needsRegisteredUsers() {
        return ItemMeanPopularityComputer.needsRegisteredUsers();
    }

    @Test
    public void testComputer(){
        initGlData();
        OpinionsProvider opinionsProvider = getOpinionsProvider();
        Kera kera = new Kera(getItemsProvider(),getUsersProvider(needsRegisteredUsers()), opinionsProvider);
        if (isComputeNewModel()) log.info("start training");
        ItemMeanPopularityComputer computer = new ItemMeanPopularityComputer(false);
        if (isComputeNewModel()){
            kera.addComputerWithComputation("IMPCTest",computer);
        }else {
            kera.addComputer("IMPCTest",computer);
        }
        log.info("start recommending");

        Map<Integer, Double> testItemVec = computer.getModel().get(0);
        Assert.assertEquals(4.259d,testItemVec.get(computer.getItemId("2959")),0.01d);
        Assert.assertEquals(4.246d,testItemVec.get(computer.getItemId("1203")),0.01d);

        long recStart = System.currentTimeMillis();
        List<KeraResultItem> resultItems = kera.recommend("IMPCTest", "90097", 5,true);
        log.info("Recommendation took: " + (System.currentTimeMillis() - recStart) + "ms");
        Assert.assertEquals("858",resultItems.get(0).getExternalId());
        Assert.assertEquals(4.3158,resultItems.get(0).getScore(),0.01);
        Assert.assertEquals("1248",resultItems.get(1).getExternalId());
        Assert.assertEquals(4.2592,resultItems.get(1).getScore(),0.01);
        Assert.assertEquals("2959",resultItems.get(2).getExternalId());
        Assert.assertEquals(4.2585,resultItems.get(2).getScore(),0.01);
        Assert.assertEquals("7502",resultItems.get(3).getExternalId());
        Assert.assertEquals(4.2474,resultItems.get(3).getScore(),0.01);
    }

    @Test
    public void testComputerSnData() {
        initSNData();
        String computerCode = "IMPCTestSn";

        OpinionsProvider opinionsProvider = getOpinionsProvider();
        Map<String, List<KeraOpinion>> ops = new HashMap<>();

        opinionsProvider.getObjects().forEach(i -> {
            List<KeraOpinion> o = ops.getOrDefault(i.getUserId(), new LinkedList<>());
            o.add(i);
            ops.put(i.getUserId(), o);
        });


        Kera kera = new Kera(getItemsProvider(), getUsersProvider(needsRegisteredUsers()), opinionsProvider);
        if (isComputeNewModel()) log.info("start training");
        KeraComputer computer = new ItemMeanPopularityComputer(true);
        computer.setNeighborhoodSize(20);
        if (isComputeNewModel()) {
            kera.addComputerWithComputation(computerCode, computer);
        } else {
            kera.addComputer(computerCode, computer);
        }
        log.info("start recommending");
        long recStart = System.currentTimeMillis();
        List<KeraResultItem> resultItems = kera.recommend(
                computerCode,
                "ffc64d83-db43-480e-ab80-80356f5fce01",
                10,
                true);

        log.info("Recommendation took: " + (System.currentTimeMillis() - recStart) + "ms");

        Assert.assertEquals("070-0009-cis-barva-paul1-v-seda-cerna-paul-cis-leva-prava-v-leva-1",resultItems.get(0).getExternalId());
        Assert.assertEquals(1393.0,resultItems.get(0).getScore(),0.01);
        Assert.assertEquals("058-0196",resultItems.get(1).getExternalId());
        Assert.assertEquals(982.0,resultItems.get(1).getScore(),0.01);
        Assert.assertEquals("058-0185",resultItems.get(2).getExternalId());
        Assert.assertEquals(967.0,resultItems.get(2).getScore(),0.01);
        Assert.assertEquals("058-0044", resultItems.get(3).getExternalId());
        Assert.assertEquals(794.0,resultItems.get(3).getScore(),0.01);
        Assert.assertEquals("070-0022-cis-barva-hewlet-v-cern-cerv-h-cis-leva-prava-v-leva-1", resultItems.get(4).getExternalId());
        Assert.assertEquals(787.0,resultItems.get(4).getScore(),0.01);
        Assert.assertEquals("070-0097-cis-barva-paul2-v-17-11-paul", resultItems.get(5).getExternalId());
        Assert.assertEquals(759.0,resultItems.get(5).getScore(),0.01);

    }
    @Test
    public void testComputerProData() {
        initProData();
        String computerCode = "IMPCTestPro";

        OpinionsProvider opinionsProvider = getOpinionsProvider();
        Map<String, List<KeraOpinion>> ops = new HashMap<>();

        opinionsProvider.getObjects().forEach(i -> {
            List<KeraOpinion> o = ops.getOrDefault(i.getUserId(), new LinkedList<>());
            o.add(i);
            ops.put(i.getUserId(), o);
        });


        Kera kera = new Kera(getItemsProvider(), getUsersProvider(needsRegisteredUsers()), opinionsProvider);
        if (isComputeNewModel()) log.info("start training");
        KeraComputer computer = new ItemMeanPopularityComputer(true);
        computer.setNeighborhoodSize(20);
        if (isComputeNewModel()) {
            kera.addComputerWithComputation(computerCode, computer);
        } else {
            kera.addComputer(computerCode, computer);
        }
        log.info("start recommending");
        long recStart = System.currentTimeMillis();
        List<KeraResultItem> resultItems = kera.recommend(
                computerCode,
                "d5da48a4-91e3-4be0-9f6f-504686be372d",
                10,
                true);

        log.info("Recommendation took: " + (System.currentTimeMillis() - recStart) + "ms");

        Assert.assertEquals("9C0FF489-1DB9-4104-8103-C140CFD11824",resultItems.get(0).getExternalId());
        Assert.assertEquals(781.0,resultItems.get(0).getScore(),0.01);
        Assert.assertEquals("C8829DF7-F4FB-4FF2-91B0-ED60F10470AA",resultItems.get(1).getExternalId());
        Assert.assertEquals(750.0,resultItems.get(1).getScore(),0.01);
        Assert.assertEquals("5D25E490-4C32-40FC-905B-D5F6ACBB9ECF",resultItems.get(2).getExternalId());
        Assert.assertEquals(733.0,resultItems.get(2).getScore(),0.01);
        Assert.assertEquals("4567F104-7088-4954-A443-34D318780D60", resultItems.get(3).getExternalId());
        Assert.assertEquals(707.0,resultItems.get(3).getScore(),0.01);
        Assert.assertEquals("8F448FAA-F6A4-4A22-8853-AFB49E281A4E", resultItems.get(4).getExternalId());
        Assert.assertEquals(522.0,resultItems.get(4).getScore(),0.01);
        Assert.assertEquals("0C01F714-2DE3-4BF5-B7C0-A905A6F06B99", resultItems.get(5).getExternalId());
        Assert.assertEquals(434.0,resultItems.get(5).getScore(),0.01);

    }

}
