package dev.kamest.kera.evaluators;

import dev.kamest.kera.evaluators.error.MAE;
import dev.kamest.kera.evaluators.precision.MAP;
import org.junit.Assert;

import java.io.IOException;

public class MAETest extends AbstractEvaluatorTest {

    @Override
    public void testEvaluator() throws IOException {
        Evaluator evaluator = new MAE();
        evaluator.setTopN(10);

        double eval = evaluator.eval(getThruthList("ratingTest"), getRecList("ratingRec"));
        Assert.assertEquals(0.7506281916580411, eval, 0.001);
    }
}
