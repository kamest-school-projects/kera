package dev.kamest.kera.evaluators;

import dev.kamest.kera.evaluators.precision.MAP;
import org.junit.Assert;

import java.io.IOException;

public class MAPTest extends AbstractEvaluatorTest {

    @Override
    public void testEvaluator() throws IOException {
        Evaluator evaluator = new MAP();
        evaluator.setTopN(10);

        double eval = evaluator.eval(getThruthList("test"), getRecList("rec"));
        Assert.assertEquals(0.2219048552514755, eval, 0.001);
    }
}
