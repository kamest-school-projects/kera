package dev.kamest.kera.evaluators;

import dev.kamest.kera.evaluators.error.RMSE;
import dev.kamest.kera.evaluators.precision.MAP;
import org.junit.Assert;

import java.io.IOException;

public class RMSETest extends AbstractEvaluatorTest {

    @Override
    public void testEvaluator() throws IOException {
        Evaluator evaluator = new RMSE();
        evaluator.setTopN(10);

        double eval = evaluator.eval(getThruthList("ratingTest"), getRecList("ratingRec"));
        Assert.assertEquals(0.9968417226150593, eval, 0.001);
    }
}
