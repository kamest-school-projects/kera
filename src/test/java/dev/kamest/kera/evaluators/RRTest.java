package dev.kamest.kera.evaluators;

import dev.kamest.kera.evaluators.precision.RR;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class RRTest extends AbstractEvaluatorTest {

    @Test
    public void testEvaluator() throws IOException {
        Evaluator evaluator = new RR();
        evaluator.setTopN(10);

        double eval = evaluator.eval(getThruthList("test"), getRecList("rec"));
        Assert.assertEquals(0.5514547459139184, eval, 0.001);
    }
}
