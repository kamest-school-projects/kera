package dev.kamest.kera.evaluators;

import dev.kamest.kera.evaluators.precision.Recall;
import org.junit.Assert;

import java.io.IOException;

public class RecallTest extends AbstractEvaluatorTest {

    @Override
    public void testEvaluator() throws IOException {
        Recall evaluator = new Recall();
        evaluator.setTopN(10);

        double eval = evaluator.eval(getThruthList("test"), getRecList("rec"));
        Assert.assertEquals(0.18541345393290157, eval, 0.001);
    }
}
